import { Photo } from "@/interfaces/PhotoDto";
import { defineStore } from "pinia";
import { ref } from "vue";

export const usePhotosStore = defineStore("photos", () => {
  const accessKey = "-HvUDQPrUj8JCIbdGoXoiBeisL0fBWg83FR8ZeYA-TU";
  const numberOfRandomPhotos = 9;
  const photos = ref<Photo[]>([]);
  const likedPhotos = ref<Photo[]>([]);
  const currentPhoto = ref<Photo>();

  const setPhotos = (data: Photo[]) => {
    photos.value.length = 0;
    photos.value.push(...data);
  };

  const setLikedPhotos = (data: Photo[]) => {
    likedPhotos.value.length = 0;
    likedPhotos.value.push(...data);
  };

  const addLikedPhoto = (data: Photo | undefined) => {
    if (!data) {
      return;
    }
    likedPhotos.value.push(data);
  };
  const removePhotoFromLiked = (data: Photo | undefined) => {
    if (!data) {
      return;
    }
    const index = likedPhotos.value.findIndex((x) => x.id === data.id);
    if (index > -1) {
      likedPhotos.value.splice(index, 1);
    }
  };

  const setCurrentPhoto = (data: Photo) => {
    currentPhoto.value = data;
  };

  const downloadImage = async (imageUrl: string) => {
    try {
      const response = await fetch(imageUrl);
      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);

      const a = document.createElement("a");
      a.href = url;
      a.download = "image.jpg";
      a.style.display = "none";

      document.body.appendChild(a);
      a.click();
      console.log("Downloaded image");

      // Clean up
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    } catch (error) {
      console.error("Error downloading image:", error);
    }
  };

  const getPhotos = async () => {
    await fetch(
      `https://api.unsplash.com/photos/random/?count=${numberOfRandomPhotos}&client_id=${accessKey}`,
    )
      .then((res) => res.json())
      .then((data) => {
        setPhotos(data);
      });
  };

  const getPhoto = async (id: string) => {
    await fetch(`https://api.unsplash.com/photos/${id}/?client_id=${accessKey}`)
      .then((res) => res.json())
      .then((data) => {
        setCurrentPhoto(data);
      });
  };

  const getPhotoBySearch = async (search: string) => {
    if (!search) {
      return;
    }
    await fetch(
      `https://api.unsplash.com/search/photos/?query=${search}&client_id=${accessKey}`,
    )
      .then((res) => res.json())
      .then((data) => {
        setPhotos(data.results);
      });
  };

  const likePhoto = async (id: string) => {
    await fetch(
      `https://api.unsplash.com/photos/${id}/like/?client_id=${accessKey}`,
      {
        method: "POST",
      },
    )
      .then((res) => res.json())
      .then((data) => {
        addLikedPhoto(data.photo);
      });
  };

  const unlikePhoto = async (id: string) => {
    await fetch(
      `https://api.unsplash.com/photos/${id}/like/?client_id=${accessKey}`,
      {
        method: "DELETE",
      },
    )
      .then((res) => res.json())
      .then((data) => {
        removePhotoFromLiked(data.photo);
      });
  };

  const getLikedPhotos = async () => {
    await fetch(
      `https://api.unsplash.com/photos/?client_id=${accessKey}&liked_by_user=true`,
    )
      .then((res) => res.json())
      .then((data) => {
        setLikedPhotos(data);
      });
  };

  const downloadPhoto = async (id: string) => {
    await fetch(
      `https://api.unsplash.com/photos/${id}/download/?client_id=${accessKey}`,
      {
        method: "GET",
      },
    )
      .then((res) => res.json())
      .then(async (data) => {
        downloadImage(data.url);
      });
  };

  return {
    accessKey,
    photos,
    currentPhoto,
    likedPhotos,
    addLikedPhoto,
    removePhotoFromLiked,
    setLikedPhotos,
    setCurrentPhoto,
    getPhotos,
    getPhoto,
    getPhotoBySearch,
    setPhotos,
    likePhoto,
    unlikePhoto,
    getLikedPhotos,
    downloadPhoto,
  };
});
